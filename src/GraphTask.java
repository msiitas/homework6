import java.util.*;

/**
 * Koostada meetod etteantud sidusa lihtgraafi minimaalse kaaluga aluspuu
 * (minimaalse toese) leidmiseks.
 * 
 * @Author Maris Siitas IA17 2016
 *
 */

public class GraphTask {

	public static void main(String[] args) {
		GraphTask a = new GraphTask();
		a.run();

	}

	public void run() {
		Graph g = new Graph("G");
		g.createRandomSimpleGraph(5, 7, 10);
		System.out.println(g);

		g.kruskal();
		
	}

	class Vertex {

		private String id;
		private Vertex next;
		private Arc first;
		private int info = 0;
		
		/**
		 * Constructor
		 * 
		 * @param s vertex name/id
		 * @param v next vertex
		 * @param e first arc
		 */
		Vertex(String s, Vertex v, Arc e) {
			id = s;
			next = v;
			first = e;
		}

		Vertex(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

	}

	class Arc {

		private String id;
		private Vertex source;
		private Vertex target;
		private Arc next;
		private Integer info;
		
		/**
		 * Constructor
		 * 
		 * @param s arc name/id
		 * @param src source vertex
		 * @param dest destination vertex
		 * @param a next arc
		 * @param w the weight of an arc
		 */
		Arc(String s, Vertex src, Vertex dest, Arc a, Integer w) {
			id = s;
			source = src;
			target = dest;
			next = a;
			info = w;

		}

		Arc(String s) {
			this(s, null, null, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

		/**
		 * @return the info (weight)
		 */
		public Integer getInfo() {
			return info;
		}

		/**
		 * @param info
		 *            the info to set
		 */
		public void setInfo(Integer info) {
			this.info = info;
		}

		/**
		 * @return the source
		 */
		public Vertex getSource() {
			return source;
		}

		/**
		 * @param source
		 *            the source to set
		 */
		public void setSource(Vertex source) {
			this.source = source;
		}

		/**
		 * @return the target
		 */
		public Vertex getTarget() {
			return target;
		}

		/**
		 * @param target
		 *            the target to set
		 */
		public void setTarget(Vertex target) {
			this.target = target;
		}

		/**
		 * Compare to function for arcs' weight
		 * 
		 * @param o arc
		 * @return greater weight
		 */
		public int compareTo(Arc o) {

			if (this.getInfo() > o.getInfo()) {
				return 1;
			}

			else if (this.getInfo() < o.getInfo()) {
				return -1;
			}

			else {
				return 0;
			}
		}

	}

	class Graph {

		private List<Arc> arcs;
		private String id;
		private Vertex first;
		private int info = 0;

		/**
		 * Constructor
		 * 
		 * @param s graph name/id
		 * @param v first vertex
		 */
		Graph(String s, Vertex v) {
			id = s;
			first = v;
		}

		Graph(String s) {
			this(s, null);
		}

		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append(id);
			sb.append(nl);
			Vertex v = first;
			while (v != null) {
				sb.append(v.toString());
				sb.append(" -->");
				Arc a = v.first;
				while (a != null) {
					sb.append(" ");
					sb.append(a.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(a.target.toString());
					sb.append(")");
					a = a.next;
				}
				sb.append(nl);
				v = v.next;
			}
			return sb.toString();
		}
		
		/**
		 *Method for creating vertex
		 *
		 * @param vid vertex name/id
		 */
		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.next = first;
			first = res;
			return res;
		}

		/**
		 *Method for creating arc
		 *
		 * @param aid arc name/id
		 * @param from source vertex
		 * @param to destination vertex
		 * @param wt weight of the arc
		 */
		public Arc createArc(String aid, Vertex from, Vertex to, int wt) {
			Arc res = new Arc(aid);
			res.next = from.first;
			from.first = res;
			res.source = from;
			res.target = to;
			res.setInfo(wt);
			return res;
		}

		/**
		 * Create a connected undirected random tree with n vertices. Each new
		 * vertex is connected to some random existing vertex.
		 * 
		 * @param n number of vertices added to this graph
		 * @param weight weight of the edge
		 */
		public void createRandomTree(int n, int weight) {
			if (n <= 0 || weight <= 0)
				return;

			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + String.valueOf(n - i));
				if (i > 0) {
					int vnr = (int) (Math.random() * i);
					int wt = (int) (Math.random() * weight + 1);
					arcs.add(createArc("a" + varray[vnr].toString() + "_" + varray[i].toString() + " = " + wt,
							varray[vnr], varray[i], wt));
					// arcs.add(createArc("a" + varray[i].toString() + "_" +
					// varray[vnr].toString() + " = " + wt, varray[i],
					// varray[vnr], wt));
				} else {
				}
			}
		}

		/**
		 * Create an adjacency matrix of this graph. Side effect: corrupts info
		 * fields in the graph
		 * 
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = first;
			while (v != null) {
				v.info = info++;
				v = v.next;
			}
			int[][] res = new int[info][info];
			v = first;
			while (v != null) {
				int i = v.info;
				Arc a = v.first;
				while (a != null) {
					int j = a.target.info;
					res[i][j]++;
					a = a.next;
				}
				v = v.next;
			}
			return res;
		}

		/**
		 * Create a connected simple (undirected, no loops, no multiple arcs)
		 * random graph with n vertices and m edges with weights.
		 * 
		 * @param n number of vertices
		 * @param m number of edges
		 * @param weight weight of the edge
		 */
		int tippudeArv;
		int kaarteArv;

		public void createRandomSimpleGraph(int n, int m, int weight) {
			arcs = new ArrayList<Arc>();
			tippudeArv = n;
			kaarteArv = m;
			if (n <= 0 || weight <= 0)
				return;
			if (n > 2500)
				throw new IllegalArgumentException("Too many vertices: " + n);
			if (m < n - 1 || m > n * (n - 1) / 2)
				throw new IllegalArgumentException("Impossible number of edges: " + m);
			if (weight > 1000)
				throw new IllegalArgumentException("Too big weight: " + weight);
			first = null;
			createRandomTree(n, weight); // n-1 edges created here
			Vertex[] vert = new Vertex[n];
			Vertex v = first;
			int c = 0;
			while (v != null) {
				vert[c++] = v;
				v = v.next;
			}
			int[][] connected = createAdjMatrix();
			int edgeCount = m - n + 1; // remaining edges

			while (edgeCount > 0) {
				int i = (int) (Math.random() * n); // random source
				int j = (int) (Math.random() * n); // random target
				int wt = (int) (Math.random() * weight + 1); // random weight
				if (i == j)
					continue; // no loops
				if (connected[i][j] != 0 || connected[j][i] != 0)
					continue; // no multiple edges
				Vertex vi = vert[i];
				Vertex vj = vert[j];
				arcs.add(createArc("a" + vi.toString() + "_" + vj.toString() + " = " + wt, vi, vj, wt));
				connected[i][j] = 1;
				// arcs.add(createArc("a" + vj.toString() + "_" + vi.toString()
				// + " = " + wt, vj, vi, wt));
				// connected[j][i] = 1;

				edgeCount--; // a new edge happily created

			}

		}
		
		 /** 
		  * A class to represent a subset for union-find
		  */
		class subset {
			int parent, rank;
		};

		/**
		 * A utility function to find set of an element i
		 * Find root and make root as parent of i (path compression)
		 * @param subsets[] arc set array
		 * @param i arc
		 * @see http://www.geeksforgeeks.org/greedy-algorithms-set-2-kruskals-minimum-spanning-tree-mst/
		 */
		int find(subset subsets[], int i) {
			// find root and make root as parent of i (path compression)
			if (subsets[i].parent != i)
				subsets[i].parent = find(subsets, subsets[i].parent);

			return subsets[i].parent;
		}

		/**
		 * A function that does union of two sets of x and y (uses union by
		 * rank)
		 * 
		 * @param subsets[] arc set array
		 * @param x source vertex
		 * @param y target vertex
		 */
		void Union(subset subsets[], int x, int y) {
			int xroot = find(subsets, x);
			int yroot = find(subsets, y);

			// Attach smaller rank tree under root of high rank tree (Union by Rank)
			if (subsets[xroot].rank < subsets[yroot].rank)
				subsets[xroot].parent = yroot;
			else if (subsets[xroot].rank > subsets[yroot].rank)
				subsets[yroot].parent = xroot;

			// If ranks are same, then make one as root and increment its rank by one
			else {
				subsets[yroot].parent = xroot;
				subsets[xroot].rank++;
			}
		}

		public void kruskal() {

			Arc result[] = new Arc[tippudeArv]; // Array that store the resultant MST
			int e = 0; // An index variable, used for result[]
			int i = 0; // An index variable, used for sorted arcs
			for (i = 0; i < tippudeArv; ++i)
				result[i] = new Arc(id);

			//Sorted arcs
			Collections.sort(arcs, new Comparator<Arc>() {
				@Override
				public int compare(Arc arc2, Arc arc1) {

					return arc2.getInfo().compareTo(arc1.getInfo());
				}
			});

			System.out.println("Sorteeritud kaared: " + arcs);
			System.out.println();
			
//			for (Arc arc : arcs) {
//
//				Vertex esimeneTipp = arc.getSource();
//				Vertex viimaneTipp = arc.getTarget();
//				int kaal = arc.getInfo();
//
//				System.out.println("Kaare " + arc + " algustipp on " + esimeneTipp + " ja lopptipp on " + viimaneTipp
//						+ ". Kaal on " + kaal);
//
//			}

			// Allocate memory for creating vertex number subsets
			subset subsets[] = new subset[tippudeArv];
			for (i = 0; i < tippudeArv; ++i)
				subsets[i] = new subset();

			// Create vertex number subsets with single elements
			for (int v = 0; v < tippudeArv; ++v) {
				subsets[v].parent = v;
				subsets[v].rank = 0;
			}

			i = 0; // Index used to pick next arc

			// Number of arcs to be taken is equal to V-1
			while (e < tippudeArv - 1) {
				
				// Pick the smallest arc and increment the index for next iteration
				Arc next_edge = new Arc(id);
				next_edge = (Arc) arcs.toArray()[i++];

				String src = next_edge.getSource().toString().replaceAll("[^0-9?!\\.]", ""); //Gets source vertex number
				String dest = next_edge.getTarget().toString().replaceAll("[^0-9?!\\.]", ""); //Gets destination vertex number

				int x = find(subsets, Integer.parseInt(src));
				int y = find(subsets, Integer.parseInt(dest));

				// If including this arc does't cause cycle, include it in result and increment the index of result for next arc
				if (x != y) {
					result[e++] = next_edge;
					Union(subsets, x, y);
				}
				// Else discard the next_edge

				System.out.println("Need on minimaalse aluspuu kaared: ");
				for (i = 0; i < e; ++i)
					System.out.println(
							result[i].getSource() + " --> " + result[i].getTarget() + " = " + result[i].getInfo());
			}

			

		}

	}

}
